package sdk.livechat.testapp;

import static sdk.livechat2.CallSession.EventIndex.MEDIA_REQ_RECEIVED;

import android.app.Activity;
import android.os.Bundle;
import android.telecom.Call;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import sdk.livechat.Configs;
import sdk.livechat.Log;
import sdk.livechat.MessageType;
import sdk.livechat.SysAddr;
import sdk.livechat.Version;
import sdk.livechat.logger.LvLogger;
import sdk.livechat2.CallSession;
import sdk.livechat2.Livechat2Api;
import sdk.livechat2.callback.IStartCompleteCallback;

public class Livechat2ApiActivity extends Activity {

// region Variables
    ViewGroup liveChatLayout, logsLayout, videoLayout, activeViewGroup;
    EditText m_LogsViewEditText;
    TextView m_LiveChatVersionTextView;
    EditText m_UrlEditText, m_UrlProxy, m_MessageEditText;
    LinearLayout m_VideoLinearLayout;
    ViewGroup m_ViewArea;

    Livechat2Api livechat2Api;
    CallSession callSession;

    String token = "eyJhbGciOiJIUzUxMiJ9.eyJjaGFubmVsIjoiQ0hBVCIsInNlc3Npb25JZCI6ImFrZHNqYWxza2FTIiwiZW52IjoiREVWIiwicGFnZUlkIjoiQUNDT1VOVFMiLCJtb2JpbGVBdXRoRmxhZyI6InRydWUiLCJjdXN0b21lck5hbWUiOiJ0ZXRoZXJmaSIsIm9yZ0lkIjoiMSIsInNraWxsIjoiQUNDT1VOVFMiLCJvcmdhbml6YXRpb24iOiJ0ZXRoZXJmaSIsImN1c3RvbWVySWQiOiI1NDY3NTEzMDUxIiwiZXhwIjo3NjIxNjUyMTQsImlhdCI6MTY1NDg2MzgzOCwianRpIjoiMmE3NTk3NDUtYjYzNi00ZWQwLThhZWUtMGU3NjNlZGNkZDZlIn0.BBcf-7MzgP40DbD5yibQSrWAqDT_rJk3FMDiMWDs8B9jSmqSysJhn526B6ypMZ_fR48ocdICxbj_v0LnTocgTw&sessionId=3a600342-a7a3-4c66-bbd3-f67de5d7096f";
    String url = "wss://calabrio.tetherfi.cloud:9443/ws-chat-proxy/ws/chat";

// endregion

// region Initialization

    /**
     * Called when the activity is created. Responsible for initializing
     * the activity's user interface and any necessary components or data
     *
     * @param savedInstanceState A Bundle containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeViews();

        try {
            logger.setNewFileHandler(Livechat2ApiActivity.this);
        }catch ( Exception ex )
        {
            ex.printStackTrace();
        }

        Log.SetLogger(logger);
        m_UrlEditText.setText( url );
        m_UrlProxy.setText( "turn:calabrio.tetherfi.cloud:3585,tetherfi,nuwan" );
    }


    /**
     * Initializes the application by setting up logging, initializing Livechat2API and Call session
     * from Livechat2 sdk
     * register event listeners, configuring media connection settings, and adding
     * audiovisual (AV) proxies.
     */
    private void initApp() {

        String temp1 = m_UrlEditText.getText().toString();
        livechat2Api = new Livechat2Api(
                new Livechat2Api.ProxyURL( temp1.trim() ),
                new Livechat2Api.UserAuth( "uid", token, "cloud" )
        );

        String temp = m_UrlProxy.getText().toString();
        String[] proxy = temp.split(",");

        callSession = new CallSession(this);

        livechat2Api.registerListener( "app", onUserMessage);
        callSession.AddCallEventHandler(MEDIA_REQ_RECEIVED, "app", onAvRequest);
        callSession.AddCallEventHandler(CallSession.EventIndex.CONNECTING_MEDIA, "app", onConnecting);
        callSession.AddCallEventHandler(CallSession.EventIndex.INCALL_MEDIA, "app", onConnected);
        callSession.AddCallEventHandler(CallSession.EventIndex.RECONNECTING_MEDIA, "app", onReconnecting);
        callSession.AddCallEventHandler(CallSession.EventIndex.RECONNECTABLE_END, "app", onReconnectableEnd);
        callSession.AddCallEventHandler(CallSession.EventIndex.REINCALL_MEDIA, "app", onReconnected );
        callSession.AddCallEventHandler(CallSession.EventIndex.ENDED, "app", onEnded);
        callSession.SetVideoEventHandler(onVideoEvents);

        livechat2Api.setStateLisetener(new Livechat2Api.ISigListener() {
            @Override
            public void onEnd(String reason) {
                Log.i( "onEnd : " + reason );
            }
            @Override
            public void onServerStateChanged(String state) {

            }
        });

         /*
        Configs.Instance.mediaConnectionMode = Configs.MediaConnMode.MediaConnModeAll;
        Configs.Instance.addAvProxy(proxy[0].trim(), proxy[1].trim(), proxy[2].trim() );
        Configs.Instance.mungSdp.enabled = true;
         */

        try{
            Configs.Instance.configByJSON( new JSONObject(
                    """
                    {
                        "type": "configs",
                        "ice": [
                            {
                            "url":"turn:calabrio.tetherfi.cloud:3585",
                            "user":"tetherfi",
                            "pass":"nuwan"
                            }
                        ],
                        "avmode": "audio",
                        "video": {
                            "width": 720,
                            "height": 720,
                            "fps": 25
                        },
                        "media_conn_mode": "all",
                        "mirror_selfview": true,
                        "android_manage_audio": true,
                        "prompt_avrequest": true,
                        "sdp_mung": {
                            "enabled": true,
                            "audio_limit": 32,
                            "video_limit": 128,
                            "session_limit": 1024,
                            "vpcodec": 8
                        },
                        "detect_gsm": false,
                        "sdpPlan" : "plan-b"
                    }
                    """
            ));}catch ( JSONException ex )
        {
            ex.printStackTrace();
        }

        Configs.Instance.addAvProxy(proxy[0].trim(), proxy[1].trim(), proxy[2].trim() );

    }

    /**
     * Initializes the views used in the activity by inflating layout resources
     */
    private void initializeViews() {
        // Inflate layout resources
        liveChatLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_livechat, null);
        logsLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_logs, null);
        videoLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_video, null);

        // Set up view visibility and initialize UI elements
        switchView(liveChatLayout);
        addContentView(logsLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        addContentView(videoLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        logsLayout.setVisibility(View.GONE);
        m_LogsViewEditText = (EditText) findViewById(R.id.txtLogs);
        m_LiveChatVersionTextView = (TextView) findViewById(R.id.lvchatversion);
        m_LiveChatVersionTextView.setText(Livechat2Api.getVersion());
        m_UrlEditText = findViewById(R.id.txtUrl);
        m_UrlProxy = findViewById( R.id.txtProxy );
        m_MessageEditText = findViewById(R.id.txtMsg);
        m_VideoLinearLayout = findViewById(R.id.vid_layout);
        m_ViewArea = (ViewGroup) findViewById(R.id.view_Area);
    }

// endregion

// region ButtonEvents


    /**
     * Called when the device's back button is pressed/back gestures.
     */
    @Override
    public void onBackPressed() {
        if (activeViewGroup == logsLayout) {
            switchView(liveChatLayout);
            return;
        }
        super.onBackPressed();
    }

    /**
     * Clears the logs view edit text field when the clear button is clicked.
     * Sets the text of the logs view edit text field to an empty string
     *
     * @param v The view representing the clear button that was clicked.
     */
    public void onClickClear(View v) {
        m_LogsViewEditText.setText("");
    }

    /**
     * Switch the active view to the logs layout when log button in clicked.
     * Switches the active view group to the logs layout
     *
     * @param v The view representing the logs button that was clicked.
     */
    public void onClickLogs(View v) {
        switchView(logsLayout);
    }

    /**
     * Log a debug message "Test start" when test button clicked.
     *
     * @param v The view representing the test button that was clicked.
     */
    public void onClickTest(View v) {
        Log.d("Test start");
    }

    /**
     * Send a user message via Livechat2Api when the send button is clicked.
     * The message is retrieved from m_MessageEditText.
     *
     * @param v The view representing the send button that was clicked.
     */
    public void onClickSend(View v) {
        livechat2Api.sendLv2Message(
                MessageType.USER_MESSAGE,
                m_MessageEditText.getText().toString(),
                new SysAddr("user", "agent"),
                null
        );

    }

    /**
     * Switch the active view to the video layout when video view button clicked.
     * Switches the active view group to the video layout, making the video view visible
     *
     * @param view The view representing the video button that was clicked.
     */
    public void onClickVideo(View view) {
        switchView(videoLayout);
    }

    /**
     * Starts an audio and video call session using livechat2 sdk when AV button is clicked
     *
     * @param view The view representing the AV button that was clicked.
     */
    public void onClickAV(View view) {
        Configs.Instance.selectedCamera = Configs.CameraLocation.FrontCamera;
        callSession.StartAv(
                "audio",
                "{ \"id\":\"agent-id\", \"title\":\"agent-name\", \"remoteAddr\":\"user:agent\" }",
                false);
    }


    private int index = 0;
    public void onClickDtmf(View v) {

        CallSession.DtmfTone[] dtmf = new CallSession.DtmfTone[]{
                CallSession.DtmfTone.NUM_0, CallSession.DtmfTone.NUM_1, CallSession.DtmfTone.NUM_3,
                CallSession.DtmfTone.NUM_6, CallSession.DtmfTone.NUM_9, CallSession.DtmfTone.POUND,
                CallSession.DtmfTone.STAR, CallSession.DtmfTone.NUM_8
        };

        callSession.sendDtmf(dtmf[index]);
        index = (index + 1) % 8;
    }

    /**
     * Start the chat session.
     * Prepares the necessary customer details, sets user authentication, and initiates the chat session.
     * This method also initializes the call session.
     *
     * @param v The view representing the clicked button
     */
    public void onClickStart(View v) {

        initApp();

        //String custDetails = "{\"pChannel\":\"\",\"pIntent\":\"demoappchat\",\"pNationality\":\"\",\"pLang\":\"\",\"pChatBotSid\":\"\",\"pAuthType\":\"\",\"pVerificationType\":\"\",\"pUserId\":\"\",\"pCIF\":\"\",\"pName\":\"Nu\",\"pGender\":\"Male\",\"pDOB\":\"\",\"pMobileNumber\":\"\",\"pCustomerTag\":\"\",\"pPIBissueDate\":\"\",\"pOTPPref\":\"\",\"pTokenSerialNumber\":\"\",\"pInvalidLogonAttempt\":\"\",\"pCustIdStatus\":\"\",\"pTokenActivated\":\"\",\"pTokenStatus\":\"\",\"pFailedTwoFACount\":\"\",\"pFirstLogiDate\":\"\",\"pfirstLoginTime\":\"\",\"plastLoginDate\":\"\",\"plastLoginTime\":\"\",\"pLeadID\":\"\",\"pApplicationID\":\"\",\"pOnboardingStatus\":\"\"}";
        String custDetails = "{\"eCustDetails\":{\"name\":\"Livechat2 TestApp User\",\"address\":\"PC\",\"pIntent\":\"51001\"}}";

        // callback must not be null
        livechat2Api.start(custDetails, true, new IStartCompleteCallback() {
            @Override
            public void onStartComplete(boolean status, String detail) {
                Log.i((status ? "success" : "fail") + ", detail :" + detail);
            }
        });

        callSession.Initialize("name");
    }

    /**
     * Ends the audiovisual (AV) media session and terminates the chat session.
     *
     * @param v The view representing the clicked button
     */
    public void onClickEnd(View v) {
        String r = "User clicked end";
        callSession.EndAv(r);
        livechat2Api.end(r);
    }

    public void onClickReconnect(View v) {
        callSession.Reconnect();
    }

// endregion

// region Livechat2 event handlers

    /**
     * Event handler for audiovisual media (AV) session request status when received
     * via Livechat2 sdk (MEDIA_REQ_RECEIVED event).
     * Logs information about the event.
     */
    private final CallSession.IEventHandler onAvRequest = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("OnAVRequest");
        }
    };

    /**
     * Event handler for audiovisual media (AV) session status when waiting for media connectivity
     * via Livechat2 sdk (CONNECTING_MEDIA event).
     * Logs information about the  event.
     */
    private final CallSession.IEventHandler onConnecting = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("onConnecting");
        }
    };

    /**
     * Event handler for audiovisual media (AV) session status when media connected
     * via Livechat2 sdk (INCALL_MEDIA event).
     * Logs information about the  event.
     */
    private final CallSession.IEventHandler onConnected = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("onConnected");
        }
    };

    /**
     * Event handler for audiovisual media (AV) session status when media disconnected
     * and waiting to reconnect via Livechat2 sdk (RECONNECTING_MEDIA event).
     * Logs information about the  event.
     */
    private final CallSession.IEventHandler onReconnecting = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("onReconnecting");
        }
    };

    private final CallSession.IEventHandler onReconnectableEnd = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("ENABLE RECONNECT");
        }
    };

    private final CallSession.IEventHandler onReconnected = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("CALL RECONNECTED");
        }
    };


    /**
     * Event handler for audiovisual media (AV) session status when media disconnected
     * via Livechat2 sdk (ENDED event).
     * Logs information about the  event.
     */
    private final CallSession.IEventHandler onEnded = new CallSession.IEventHandler() {
        @Override
        public void handle(CallSession.EventIndex event, String param1, String param2) {
            Log.i("onEnded");
        }
    };

// endregion

// region Listeners

    /**
     * Message listener for user messages received via Livechat2 sdk.
     * Logs information about the received user message, including its content and sender.
     */
    private final Livechat2Api.IMessageListener onUserMessage = new Livechat2Api.IMessageListener() {
        @Override
        public void onMessage(MessageType type, String content, String from) {
            Log.i("User message : " + content);
            Log.i("From : " + from);
        }
    };

    /**
     * Listener for audiovisual (AV) events received for adding to removing video via Livechat2 sdk.
     * Handles the addition and removal of video views during an AV session.
     */
    private final CallSession.IAvListener onVideoEvents = new CallSession.IAvListener() {
        @Override
        public void OnVideoViewAdded(SurfaceView view, String type, String id) {
            Log.i("Video received, type=" + type + ", id=" + id + ", hash=" + id.hashCode());
            if (type.equals("local")) {
                Log.i("local video ignored");
                return;
            }

            view.setId(id.hashCode());
            switchView(videoLayout);
            m_VideoLinearLayout.addView(view);
            Log.i("Id of the view is " + view.getId());
        }

        @Override
        public void OnVideoViewRemoved(String id) {

        }
    };

// endregion

// region Helpers

    /**
     * Switches the active view group to the specified view group.
     * Hides the currently active view group (if any), and makes the specified view group visible.
     *
     * @param v The view group to switch to.
     */
    private void switchView(ViewGroup v) {
        // Hide the currently active view group (if any)
        if (activeViewGroup != null) {
            activeViewGroup.setVisibility(View.GONE);
        }
        setContentView(v);
        activeViewGroup = v;
        v.setVisibility(View.VISIBLE);
    }

    /**
     * Adds a log message to the logs view on the UI thread.
     * Appends the specified message to the logs view edit text field in a thread-safe manner
     *
     * @param msg The message to be added to the logs view.
     */
    public void AddLogMessage(String msg) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_LogsViewEditText.append(msg + "\n");
            }
        });
    }

// endregion

// region Log Level Handler

    /**
     * Custom logger implementation for handling different log levels.
     * Logs information, debug, error, and warning messages.
     */
    LvLogger logger = new LvLogger() {



        @Override
        public void i(String tag, String sid, String msg, String time) {
            AddLogMessage(time + " INFO {" + sid + "} " + tag + ": " + msg);
            super.i(tag, sid, msg, time);
        }

        @Override
        public void d(String tag, String sid, String msg, String time) {
            AddLogMessage(time + " DEBUG {" + sid + "} " + tag + ": " + msg);
            super.d(tag, sid, msg, time);
        }

        @Override
        public void e(String tag, String sid, String msg, String time) {
            AddLogMessage(time + " ERROR {" + sid + "} " + tag + ": " + msg);
            super.e(tag, sid, msg, time);
        }

        @Override
        public void w(String tag, String sid, String msg, String time) {
            AddLogMessage(time + " WARN {" + sid + "} " + tag + ": " + msg);
            super.w(tag, sid, msg, time);
        }
    };




// endregion
}
