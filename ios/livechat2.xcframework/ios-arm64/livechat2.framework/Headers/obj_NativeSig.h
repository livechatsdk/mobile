//
//  obj_NativeSig.h
//  livechatnative
//
//  Created by Nuwan Abeysinghe on 1/8/20.
//  Copyright © 2020 ___NUWAN_ABEYSINGHE___. All rights reserved.
//

#ifndef obj_NativeSig_h
#define obj_NativeSig_h

@protocol SigListener;
@class MessageType;
@class SysAddr;
@class UserAuth;

@interface ObjcNativeSig : NSObject
{
}

-(instancetype) initWithListener:( id<SigListener>)listener AndUrl:(NSString*)url userAuth:(UserAuth*)userAuth;

-(void) setStateListener:(id<SigListener>)listener;

-(void) registerListener:(NSString*)regid listener:(void(^)(MessageType*,NSString*,NSString*))listener;

//-(void) setUser:(NSString*)user AndAuth:(NSString*)auth;
//-(void) setUser:(NSString*)user AndAuth:(NSString*)auth forMode:(NSString*)mode;

-(void)start:(NSString*)sessionInfo WithFetch:(BOOL)fetch
    sendStatus:(void(^)( BOOL, NSString* ))sendStatus
    responseTypes:( NSArray* )respTypes onComplete:(void(^)(BOOL, NSString*))onComplete;

-(void)open:(NSString*)session WithFetch:(BOOL)fetch;

-(NSString*)getSessionId;

-(int)sendMessage:( MessageType*)type
       content:(NSString*)content to:(SysAddr*)to attribs:(NSDictionary*)attribs
       sendStatus:(void(^)( int, BOOL, NSString*))sendStatus
       responseTypes:( NSArray* )respTypes
       onComplete:(void(^)(BOOL, MessageType*, NSString*, NSString* ))onComplete;

-(void) setUserAuth:(UserAuth*)userAuth;

-(void) endWithReason:(NSString*)reason;

-(void) onNewMessage:(NSString*)type content:(NSString*)content from:(NSString*)from;

+(NSString*) getVersionStr;

@end


#endif /* obj_NativeSig_h */
