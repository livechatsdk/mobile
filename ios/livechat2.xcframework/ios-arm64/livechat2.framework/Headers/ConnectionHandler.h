//
//  ConnectionHandler.h
//  sigv2
//
//  Created by Nuwan Abeysinghe on 26/7/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define MAX_MESSAGE_SIZE 60000

extern void livechat_conn_handler_enqueue( const char* msg, uint32_t length );
extern uint32_t livechat_conn_handler_dequeue( char* outBuff, uint32_t buffSize );
extern NSString* read_nstring_dequeue(void);
void write_nstring_enqueue( NSString* msgstr );

extern char conn_handler_outBuffer[MAX_MESSAGE_SIZE];

@interface ConnectionHandler : NSObject

-(instancetype)initWithUrl:(NSString*)url userid:(NSString*)userid;
-(void)closeWithReason:(NSString*)reason;

@end

NS_ASSUME_NONNULL_END
