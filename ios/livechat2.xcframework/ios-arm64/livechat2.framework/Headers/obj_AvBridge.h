//
//  obj_AvBridge.h
//  livechatnative
//
//  Created by Nuwan Abeysinghe on 30/7/20.
//  Copyright © 2020 ___NUWAN_ABEYSINGHE___. All rights reserved.
//

#ifndef obj_AvBridge_h
#define obj_AvBridge_h

#import <Foundation/Foundation.h>

/*
@protocol RTCAvInterface <NSObject>
-(BOOL)initAv:(NSString*)mode;
-(BOOL)startAv:(NSString*)sid;
-(void)offer;
-(void)endAv:(NSString*)reason;
-(void)setMute:(BOOL)set;
-(void)setLoud:(BOOL)set;
-(void)setEnableVideo:(BOOL)enable includeRemote:(BOOL)remote;
-(void)renegotiate;
@end
*/
 
@protocol RTCAvInterface;

@interface ObjcAvBridge : NSObject
{
}

-(instancetype) initWithAvRtc:(id<RTCAvInterface>)avrtc;

-(void)sigMessageFromAv:(NSString*)msg;
-(void)setMessageHandlerForAv:(void(^)(NSString*))handle;
-(void)onEnd:(NSString*)type desc:(NSString*)desc;
-(void)onMediaDisconnect;
-(void)onMediaReconnect;
-(void)onMediaReconnectableEnd;
-(void)onStartSuccess;
-(void)onStartFailure:(NSString*)messgae;

@end

#endif /* obj_AvBridge_h */
