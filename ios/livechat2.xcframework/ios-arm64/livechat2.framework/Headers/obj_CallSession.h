//
//  obj_CallSession.h
//  livechatnative
//
//  Created by Nuwan Abeysinghe on 31/7/20.
//  Copyright © 2020 ___NUWAN_ABEYSINGHE___. All rights reserved.
//

#ifndef obj_CallSession_h
#define obj_CallSession_h


@interface ObjCallSession : NSObject
{
}

/*
public static interface IEventHandler{
    void handle( EventIndex event, String param1, String param2 );
}

public static interface InnerEventHandler{
    void handle( int event, String param1, String param2 );
}

public void test( InnerEventHandler test )
{}

public static interface IAvListener
{
    void OnVideoViewAdded(SurfaceView view, String type, String id );
    void OnVideoViewRemoved( String id );
}
*/

-(instancetype) init;
-(void)addHandlerForEvent:(int32_t)eventIndex  regid:(NSString*)regid  callback:(void(^)(int, NSString*, NSString*))callback;
-(void)removeHandlerForEvent:(int)eventIndex  regid:(NSString*)regid;
-(void)startAvWithMode:(NSString*)mode  andRemoteObj:(NSString*)remoteUserObj;
-(void)endAvWithReason:(NSString*)reason;
-(void)setUserResponse:(int)type withParam:(NSString*) param;
-(void)InitializeWithUserId:(NSString*)userid;

@end

#endif /* obj_CallSession_h */
