//
//  DeviceInfo.h
//  livechat
//
//  Created by Nuwan Abeysinghe on 14/12/18.
//  Copyright © 2018 ___NUWAN_ABEYSINGHE___. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceInfo : NSObject
{
}

+(NSString*) getInfoWithVer:(NSString*)version;

@end

NS_ASSUME_NONNULL_END
