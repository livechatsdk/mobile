//
//  HttpHandler.h
//  livechat2
//
//  Created by Nuwan Abeysinghe on 4/4/22.
//  Copyright © 2022 ___NUWAN_ABEYSINGHE___. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HttpHandler : NSObject
{
}

-(instancetype)init;

-(void) uploadFile:(nonnull NSURLRequest *)request fromData:(nullable NSData *)bodyData handler:(void(^)(BOOL, NSString* ))completionHandler;
-(void) downloadFile:(NSURLRequest *)request handler:(void(^)(BOOL, NSURL* ))completionHandler;

@end

NS_ASSUME_NONNULL_END
