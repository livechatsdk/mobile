//
//  ViewController.swift
//  SampleLegacyApp
//
//  Created by Nuwan Abeysinghe on 5/8/20.
//  Copyright © 2020 Nuwan Abeysinghe. All rights reserved.
//

import UIKit
import livechat2
//import Starscream



public let TOKEN:String = "eyJhbGciOiJIUzUxMiJ9.eyJjaGFubmVsIjoiQ0hBVCIsInNlc3Npb25JZCI6ImFrZHNqYWxza2FTIiwiZW52IjoiREVWIiwicGFnZUlkIjoiQUNDT1VOVFMiLCJtb2JpbGVBdXRoRmxhZyI6InRydWUiLCJjdXN0b21lck5hbWUiOiJ0ZXRoZXJmaSIsIm9yZ0lkIjoiMSIsInNraWxsIjoiQUNDT1VOVFMiLCJvcmdhbml6YXRpb24iOiJ0ZXRoZXJmaSIsImN1c3RvbWVySWQiOiI1NDY3NTEzMDUxIiwiZXhwIjo3NjIxNjUyMTQsImlhdCI6MTY1NDg2MzgzOCwianRpIjoiMmE3NTk3NDUtYjYzNi00ZWQwLThhZWUtMGU3NjNlZGNkZDZlIn0.BBcf-7MzgP40DbD5yibQSrWAqDT_rJk3FMDiMWDs8B9jSmqSysJhn526B6ypMZ_fR48ocdICxbj_v0LnTocgTw";

// 34.142.197.160:8888/ws-chat-proxy/testapp2.html
// calabrio.tetherfi.cloud:9443
public let SIG_URL:String = "wss://calabrio.tetherfi.cloud:9443/ws-chat-proxy/ws/chat"; //?token=" + TOKEN + "&authType=cloud";

public let CONFIGS:[String : Any] = [
    
        "type": "configs",
        "ice": Array<Any>(
            arrayLiteral:
                [
                    "url":"turn:calabrio.tetherfi.cloud:3585",
                    "user":"tetherfi",
                    "pass":"nuwan"
                ]
            ),
        "avmode": "audio",
        "video": [
            "width": 720,
            "height": 720,
            "fps": 25
        ],
        "media_conn_mode": "all",
        "mirror_selfview": true,
        "android_manage_audio": true,
        "prompt_avrequest": true,
        "sdp_mung": [
            "enabled": true,
            "audio_limit": 32,
            "video_limit": 128,
            "session_limit": 1024,
            "vpcodec": 8
        ],
        "detect_gsm": false,
        "sdpPlan" : "plan-b",
        //"sdpPlan" : "unified-plan"
];



class ViewController: UIViewController, SigListener {
    
    //@IBOutlet var m_UserId:UITextField!;
    //@IBOutlet var m_Token:UITextField!;
    @IBOutlet var m_Url:UITextField!;
    //@IBOutlet var m_Remote:UITextField!;
    @IBOutlet var m_IsVideo:UISwitch!;
    @IBOutlet var m_VideoLayout:UIStackView!;
    @IBOutlet var m_Version:UILabel!;
    @IBOutlet var m_Message:UILabel!;
    
    let app:Livechat2Api = Livechat2Api();
    var call:CallSession?;
    var foundUser:UserFound?;

    override func viewDidLoad()
    {
        super.viewDidLoad()
        //
        m_Url.text = SIG_URL;
   
        m_IsVideo.setOn( false, animated: true );
        m_Version.text = livechat2.Livechat2Api.getVersion();
        
        Configs.instance()?.config(byJSON: CONFIGS );
        
        call = CallSession(userid: "uid" );
        
        call!.AddCallEventHandler( index: EventIndex.MEDIA_REQ_RECEIVED, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: MEDIA_REQ_RECEIVED" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.CONNECTING_MEDIA, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: CONNECTING_MEDIA" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.INCALL_MEDIA, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: INCALL_MEDIA" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.RECONNECTING_MEDIA, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: RECONNECTING_MEDIA" );
                self.setLog(str: "RECONNECTING_MEDIA" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.RECONNECTABLE_END, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: RECONNECTABLE_END" );
                self.setLog(str: "RECONNECTABLE_END" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.REINCALL_MEDIA, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: REINCALL_MEDIA" );
                self.setLog(str: "REINCALL_MEDIA" );
            }
        );
        call!.AddCallEventHandler( index: EventIndex.ENDED, regid: "app", callback:
            { (event:EventIndex, _:String?, _:String?) in
                NSLog( "App: ENDED" );
            }
        );
        
        // setupfilelogging();
        
    }
    
    private func incomingMessageHandler( optype:MessageType?, content:String?, from:String? )
    {
        setLog( str:( optype!.name + " | " + content! + " | " + from! ) );
        
        if( optype == nil || optype! == MessageType.UNKNOWN )
        {
            NSLog( "App: Message type not found");
            return;
        }
        
        let mtype = optype!;
        
        if( mtype == MessageType.USER_MESSAGE )
        {
            if( content == nil ){
                NSLog( "App: Message content not found");
                return;
            }
            
            let userMessage:UserMessage? = UserMessage.deserialize( str: content! );
            if( userMessage == nil )
            {
                NSLog( "Deserialize failed" );
                return;
            };
            
            NSLog( "App: Received '%@' from '%@'", userMessage!.message_content, from! );
        }
        else if( mtype == MessageType.USER_FOUND )
        {
            NSLog( "App: Agent connected '%@' from '%@'", content!, from! );
        }
    }
    
    @IBAction func onClickStart()
    {
        app.setStateLisetener(listener: self);
        
        app.initialize( proxyUrl: ProxyURL(wsurl: m_Url.text!),
                        userAuth: UserAuth(userid: "uid", authToken: TOKEN, authType: "cloud") );
        app.registerListener(regid: "app", listener: incomingMessageHandler );
        
        app.start(userObject: "{\"eCustDetails\":{\"name\":\"Lv2ios TestApp User\",\"address\":\"PC\",\"pIntent\":\"51001\"}}",
                  fetch: true, onComplete: { ( success : Bool, err : String? ) in
                NSLog( "App: Start %@", ( success ? "SUCCESS" : ("FAILED with " + (err ?? "-") ) ) );
            } );
    }
    
    @IBAction func onClickEnd()
    {
        app.end(reason: "Sig End by user" );
    }
    
    @IBAction func onClickSendText()
    {
        let ret = app.sendLv2Message(
            type: MessageType.USER_MESSAGE,
            content: "{ \"text\" : \"Hello\" }",
            to: SysAddr( type: "user", id : "002002" ),
            onSent: {( mid:Int32, status:Bool, reason:String?) -> Void in
                NSLog( "App: Message %d acked with %d", mid, status );
            }
        );
        
        NSLog( "App: mid = %d", ret );
    }
    
    @IBAction func onClickStartAV()
    {
        if( m_IsVideo.isOn )
        {
            call?.SetVideoEventHandler(videoEventHandler:  VideoHandler( parent: m_VideoLayout ) );
        }
        
        let remoteObj = "{ \"id\":\"uid\", \"title\":\"testName\", \"remoteAddr\":\"user:agent\" }";
        
        call?.StartAv(mode: ( m_IsVideo.isOn ? "video": "audio" ), remoteUserObj: remoteObj );
    }
    
    @IBAction func onReconnect()
    {
        call?.Reconnect();
    }
    
    var index = 0;
    @IBAction func onClickSendDtmf()
    {
        let dtmf = [ DtmfTone.NUM_0, DtmfTone.NUM_1, DtmfTone.NUM_2,
                     DtmfTone.NUM_3, DtmfTone.NUM_4, DtmfTone.NUM_5 ];
        
        call?.SendDTMF( dtmf: dtmf[index] );
        index = (index + 1) % 5;
    }
    
    // SigListener
    func onEnd(reason: String)
    {
        NSLog( "onEnd %@", reason );
    }

    func onServerStateChanged(state: String)
    {
        
    }
    
/*
    @IBAction func onClickQueue()
    {
        let findUser = FindUserAgent( channel: "textchat", customerInfo:"", intent:"metrobankmaker" );
        app.RequestAgent(param:findUser, onComplete:{ ( result: RequestAgentResult ) in
            switch result
            {
            case .userFound( let user ):
                self.foundUser = user;
                NSLog( "App: user found, %@", user.serialize() );
                break;
            case .userNotFound( let notuser ):
                NSLog( "App: user not found %@", notuser.serialize() );
                break;
            case .errMessage( let msg ):
                NSLog( "App: error message %@", msg );
                break;
            @unknown default:
                NSLog( "App: unexpected response for find-user" );
            }
        } );
    }
*/
    
    @IBAction func onClickEndAV()
    {
        app.end(reason: "AV End by User" );
    }
    
    func setLog( str:String )
    {
        m_Message.text = str;
    }
    
}

class VideoHandler : IAvListener
{
    let m_Parent:UIView;
    
    init( parent:UIView )
    {
        m_Parent = parent;
    }
    
    func OnVideoViewAdded(view: UIView, type: String, id: String)
    {
        m_Parent.addSubview( view );
    }
    
    func OnVideoViewRemoved(id: String) {

    }
}

