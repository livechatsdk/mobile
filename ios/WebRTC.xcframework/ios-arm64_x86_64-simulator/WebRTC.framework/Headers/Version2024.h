//
//  Version2024.h
//

#import <Foundation/Foundation.h>

#import <WebRTC/RTCMacros.h>

NS_ASSUME_NONNULL_BEGIN

RTC_OBJC_EXPORT
@interface Version2024 : NSObject

+(NSString*) verStr;
+(NSString*) jsonStr;
+(NSString*) commitMsg;
+(void) print;

@end

NS_ASSUME_NONNULL_END
